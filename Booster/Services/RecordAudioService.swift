//
//  RecordAudioService.swift
//  Booster
//
//  Created by Serhii Petrishenko on 19.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import AVFoundation
import UIKit

protocol RecordAudioServiceProtocol {
  var delegate: RecordAudioServiceDelegate? { get set }
  var canRecording: Bool { get }
  
  func record()
  func pause()
  func stop()
}

protocol RecordAudioServiceDelegate: class {
  func recordAudioServiceDidAuthorize(succeeded: Bool)
  func recordAudioServiceBeginInterruption(_ service: RecordAudioServiceProtocol)
  func recordAudioServiceEndInterruption(_ service: RecordAudioServiceProtocol)
}

class RecordAudioService {
  
  weak var delegate: RecordAudioServiceDelegate?
  var canRecording: Bool {
    return self.recordPermission == .granted
  }
  
  private let fileUrl: URL
  private var notificationService: NotificationServiceProtocol
  private lazy var recorder: AVAudioRecorder? = {
    
    let settings = [
      AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
      AVSampleRateKey: 12000,
      AVNumberOfChannelsKey: 1,
      AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
    ]
    
    guard let recorder = try? AVAudioRecorder(url: self.fileUrl, settings: settings) else { return nil }
    return recorder
  }()
  
  private var recordPermission: AVAudioSession.RecordPermission {
    return AVAudioSession.sharedInstance().recordPermission
  }
  
  init(fileUrl: URL) {
    self.fileUrl = fileUrl
    self.notificationService = NotificationService()
    self.notificationService.delegate = self
  }
}

// MARK: - RecordAudioServiceProtocol
extension RecordAudioService: RecordAudioServiceProtocol {
  
  func record() {
    self.setupSession()
    self.recorder?.record()
    UIApplication.shared.beginReceivingRemoteControlEvents()
    self.notificationService.startMonitoringAudioSessionInterruption()
  }
  
  func pause() {
    self.recorder?.pause()
  }
  
  func stop() {
    self.recorder?.stop()
    UIApplication.shared.endReceivingRemoteControlEvents()
    self.notificationService.stopMonitoringAudioSessionInterruption()
  }
}

// MARK: - NotificationServiceDelegate
extension RecordAudioService: NotificationServiceDelegate {
  
  func notificationServiceAudioSessionBeginInterruption() {
    self.delegate?.recordAudioServiceBeginInterruption(self)
  }
  
  func notificationServiceAudioSessionEndInterruption() {
    self.delegate?.recordAudioServiceEndInterruption(self)
  }
}

// MARK: - Private methods
private extension RecordAudioService {
  
  func setupSession() {
    do {
      let audioSession = AVAudioSession.sharedInstance()
      try audioSession.setCategory(.playAndRecord)
      try audioSession.setActive(true)
      audioSession.requestRecordPermission { (allowed) in
        print(allowed)
      }
    } catch {
      print(error)
    }
    if self.recordPermission == .denied {
      self.delegate?.recordAudioServiceDidAuthorize(succeeded: false)
    }
  }
}
