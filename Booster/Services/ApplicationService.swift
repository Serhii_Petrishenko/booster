//
//  ApplicationService.swift
//  Booster
//
//  Created by Serhii Petrishenko on 18.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import Foundation

protocol ApplicationServiceProtocol {
  var delegate: ApplicationServiceDelegate? { get set }
  
  func playOrPauseSleepTimer()
  func stopAlarm()
  
  func setSleepTimerAction(_ action: SleepTimerAction)
  func setAlarmTime(_ time: Date)
  
  func getApplicationState() -> ApplicationState
  func getSleepTimerAction() -> SleepTimerAction
  func getSleepTimerState() -> SleepTimerState
  func getAlarmTime() -> Date
}

protocol ApplicationServiceDelegate: class {
  func applicationService(didChangeApplicationState state: ApplicationState)
  func applicationService(didChangeSleepTimerAction action: SleepTimerAction)
  func applicationService(didChangeSleepTimerState state: SleepTimerState)
  func applicationService(didChangeAlarmTime time: Date)
  func applicationServiceDidFailAuthorization()
  func applicationServiceDidGoAlarmOff()
}

class ApplicationService {
  
  weak var delegate: ApplicationServiceDelegate?
  private var sleepTimerService: SleepTimerServiceProtocol
  private var alarmService: AlarmServiceProtocol
  private let notificationService: NotificationServiceProtocol
  
  private struct C {
    static let soundUrl = URL(fileURLWithPath: Bundle.main.path(forResource: "nature", ofType: "m4a")!)
    static let recordUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("recordExample.m4a")
    static let alarmUrl = URL(fileURLWithPath: Bundle.main.path(forResource: "alarm", ofType: "m4a")!)
  }
  
  private var applicationState: ApplicationState {
    didSet {
      self.delegate?.applicationService(didChangeApplicationState: self.applicationState)
    }
  }
  
  init() {
    self.sleepTimerService = SleepTimerService(soundUrl: C.soundUrl, recordUrl: C.recordUrl)
    self.alarmService = AlarmService(soundUrl: C.alarmUrl)
    self.applicationState = .iddle
    self.notificationService = NotificationService()
    self.sleepTimerService.delegate = self
    self.alarmService.delegate = self
    self.notificationService.requestUserNotificationAuthorization()
  }
}

// MARK: - ApplicationServiceProtocol
extension ApplicationService: ApplicationServiceProtocol {
  
  func playOrPauseSleepTimer() {
    if self.applicationState == .iddle,
      self.sleepTimerService.action.canPlaySound ||
      self.sleepTimerService.canRecording {
      self.alarmService.startRunning()
    }
    
    switch self.sleepTimerService.state {
    case .playing:
      self.sleepTimerService.pausePlayingAudio()
    case .pausedPlaying:
      self.sleepTimerService.startPlayingAudio()
    case .recording:
      self.sleepTimerService.pauseRecordingAudio()
    case .pausedRecording:
      self.sleepTimerService.startRecordingAudio()
    case .stopped:
      if self.sleepTimerService.action.canPlaySound {
        self.sleepTimerService.startPlayingAudio()
      } else {
        self.sleepTimerService.startRecordingAudio()
      }
    }
  }
  
  func stopAlarm() {
    self.alarmService.stopRunning()
    self.sleepTimerService.action = .off
  }
  
  func setSleepTimerAction(_ action: SleepTimerAction) {
    guard action != self.sleepTimerService.action else { return }
    let state = self.sleepTimerService.state
    self.sleepTimerService.action = action
    if !action.canPlaySound, state == .playing || state == .pausedPlaying {
      self.sleepTimerService.stopPlayingAudio()
      self.sleepTimerService.pauseRecordingAudio()
    } else if state == .recording || state == .pausedRecording {
      self.sleepTimerService.stopRecordingAudio()
      self.sleepTimerService.pausePlayingAudio()
    }
  }
  
  func setAlarmTime(_ time: Date) {
    self.alarmService.time = time
  }
  
  func getApplicationState() -> ApplicationState {
    return self.applicationState
  }
  
  func getSleepTimerAction() -> SleepTimerAction {
    return self.sleepTimerService.action
  }
  
  func getSleepTimerState() -> SleepTimerState {
    return self.sleepTimerService.state
  }
  
  func getAlarmTime() -> Date {
    return self.alarmService.time
  }
}

extension ApplicationService: SleepTimerServiceDelegate {
  
  func sleepTimerServiceRecordAudioDidFailAuthorization() {
    self.delegate?.applicationServiceDidFailAuthorization()
  }
  
  func sleepTimerService(didChangeAction action: SleepTimerAction) {
    self.delegate?.applicationService(didChangeSleepTimerAction: action)
  }
  
  func sleepTimerService(didChangeState state: SleepTimerState) {
    self.applicationState.update(with: state)
    self.delegate?.applicationService(didChangeSleepTimerState: state)
  }
}

// MARK: - AlarmServiceDelegate
extension ApplicationService: AlarmServiceDelegate {
  
  func alarmService(didChangeTime time: Date) {
    self.delegate?.applicationService(didChangeAlarmTime: time)
  }
  
  func alarmService(didChangeState state: AlarmState) {
    switch state {
    case .wentOff:
      self.sleepTimerService.stopRecordingAudio()
      self.sleepTimerService.stopPlayingAudio()
      self.alarmService.startPlayingAudio()
      self.delegate?.applicationServiceDidGoAlarmOff()
      self.notificationService.sendNotification()
    default:
      break
    }
    self.applicationState.update(with: state)
  }
}
