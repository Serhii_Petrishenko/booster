//
//  PlayAudioService.swift
//  Booster
//
//  Created by Serhii Petrishenko on 19.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import AVFoundation
import UIKit

protocol PlayAudioServiceProtocol {
  var delegate: PlayAudioServiceDelegate? { get set }
  
  func play()
  func pause()
  func stop()
}

protocol PlayAudioServiceDelegate: class {
  func playAudioServiceBeginInterruption(_ service: PlayAudioServiceProtocol)
  func playAudioServiceEndInterruption(_ service: PlayAudioServiceProtocol)
}

class PlayAudioService {
  
  weak var delegate: PlayAudioServiceDelegate?
  var isPlaying: Bool {
    return self.audioPlayer?.isPlaying == true
  }
  
  private let fileUrl: URL
  private var notificationService: NotificationServiceProtocol
  private var audioPlayer: AVAudioPlayer?
  
  init(fileUrl: URL) {
    self.fileUrl = fileUrl
    self.notificationService = NotificationService()
    self.notificationService.delegate = self
  }
  
}

// MARK: - PlayAudioServiceProtocol
extension PlayAudioService: PlayAudioServiceProtocol {
  
  func play() {
    self.setupSession()
    self.setupPlayerIfNeeded()
    self.audioPlayer?.play()
    UIApplication.shared.beginReceivingRemoteControlEvents()
    self.notificationService.startMonitoringAudioSessionInterruption()
  }
  
  func pause() {
    self.audioPlayer?.pause()
  }
  
  func stop() {
    self.audioPlayer?.stop()
    self.audioPlayer = nil
    UIApplication.shared.endReceivingRemoteControlEvents()
    self.notificationService.stopMonitoringAudioSessionInterruption()
  }
}

// MARK: - NotificationServiceDelegate
extension PlayAudioService: NotificationServiceDelegate {
  
  func notificationServiceAudioSessionBeginInterruption() {
    self.delegate?.playAudioServiceBeginInterruption(self)
  }
  
  func notificationServiceAudioSessionEndInterruption() {
    self.delegate?.playAudioServiceEndInterruption(self)
  }
}

// MARK: - Private methods
private extension PlayAudioService {
  
  func setupPlayerIfNeeded() {
    guard self.audioPlayer == nil else { return }
    guard let audioPlayer = try? AVAudioPlayer(contentsOf: self.fileUrl) else { return }
    audioPlayer.numberOfLoops = -1
    self.audioPlayer = audioPlayer
  }
  
  func setupSession() {
    do {
      let audioSession = AVAudioSession.sharedInstance()
      try audioSession.setCategory(.playback)
      try audioSession.setActive(true)
    } catch {
      print(error)
    }
  }
}
