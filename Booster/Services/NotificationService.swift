//
//  NotificationService.swift
//  Booster
//
//  Created by Serhii Petrishenko on 19.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import AVFoundation
import UserNotifications

protocol NotificationServiceProtocol {
  var delegate: NotificationServiceDelegate? { get set }
  
  func requestUserNotificationAuthorization()
  func sendNotification()
  func startMonitoringAudioSessionInterruption()
  func stopMonitoringAudioSessionInterruption()
}

protocol NotificationServiceDelegate: class {
  func notificationServiceAudioSessionBeginInterruption()
  func notificationServiceAudioSessionEndInterruption()
}

class NotificationService {
  
  weak var delegate: NotificationServiceDelegate?
  
  private var notificationCenter: NotificationCenter {
    return NotificationCenter.default
  }
  
  private var userNotificationCervice: UNUserNotificationCenter {
    return UNUserNotificationCenter.current()
  }
}

// MARK: - NotificationServiceProtocol
extension NotificationService: NotificationServiceProtocol {
  
  func requestUserNotificationAuthorization() {
    let options: UNAuthorizationOptions = [.alert, .sound, .badge]
    userNotificationCervice.requestAuthorization(options: options) { (allowed, error) in
      print(allowed)
      if let error = error {
        print(error)
      }
    }
  }
  
  func sendNotification() {
    let content = UNMutableNotificationContent()
    content.title = "Alarm Went Of"
    
    let request = UNNotificationRequest(identifier: "Local notification", content: content, trigger: nil)
    self.userNotificationCervice.add(request)
  }
  
  func startMonitoringAudioSessionInterruption() {
    self.addObserver()
  }
  
  func stopMonitoringAudioSessionInterruption() {
    self.removeObserver()
  }
}

// MARK: - Private methods
private extension NotificationService {
  
  func addObserver() {
    self.notificationCenter.addObserver(self, selector: #selector(handleInterruption(notification:)), name: AVAudioSession.interruptionNotification, object: nil)
  }
  
  func removeObserver() {
    self.notificationCenter.removeObserver(self)
  }
  
  @objc func handleInterruption(notification: Notification) {
    guard let userInfo = notification.userInfo,
      let typeValue = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt,
      let type = AVAudioSession.InterruptionType(rawValue: typeValue) else {
        return
    }
    
    switch type {
    case .began:
      self.delegate?.notificationServiceAudioSessionBeginInterruption()
    case .ended:
      guard let optionsValue = userInfo[AVAudioSessionInterruptionOptionKey] as? UInt else { return }
      let options = AVAudioSession.InterruptionOptions(rawValue: optionsValue)
      if options.contains(.shouldResume) {
        self.delegate?.notificationServiceAudioSessionEndInterruption()
      }
    @unknown default:
      break
    }
  }
}
