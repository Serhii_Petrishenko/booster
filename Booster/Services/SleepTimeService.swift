//
//  AudioService.swift
//  Booster
//
//  Created by Serhii Petrishenko on 18.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import AVFoundation

protocol SleepTimerServiceProtocol {
  var delegate: SleepTimerServiceDelegate? { get set }
  var action: SleepTimerAction { get set }
  var state: SleepTimerState { get }
  var canRecording: Bool { get }
  
  func startPlayingAudio()
  func pausePlayingAudio()
  func stopPlayingAudio()
  func startRecordingAudio()
  func pauseRecordingAudio()
  func stopRecordingAudio()
}

protocol SleepTimerServiceDelegate: class {
  func sleepTimerService(didChangeAction action: SleepTimerAction)
  func sleepTimerService(didChangeState state: SleepTimerState)
  func sleepTimerServiceRecordAudioDidFailAuthorization()
}

class SleepTimerService {
  
  weak var delegate: SleepTimerServiceDelegate?
  
  var action: SleepTimerAction {
    didSet {
      self.counter = 0
      self.delegate?.sleepTimerService(didChangeAction: self.action)
    }
  }
  
  private(set) var state: SleepTimerState {
    didSet {
      self.delegate?.sleepTimerService(didChangeState: self.state)
    }
  }
  
  var canRecording: Bool {
    return self.recordAudioService.canRecording
  }
  
  private var timer: Timer?
  private var counter = 0
  private var playAudioService: PlayAudioServiceProtocol
  private var recordAudioService: RecordAudioServiceProtocol
  
  private var duration: Int? {
    switch self.action {
    case .off: return nil
    case .on(let minutes): return minutes * 60
    }
  }
  
  init(soundUrl: URL, recordUrl: URL) {
    self.action = .off
    self.state = .stopped
    self.playAudioService = PlayAudioService(fileUrl: soundUrl)
    self.recordAudioService = RecordAudioService(fileUrl: recordUrl)
    self.playAudioService.delegate = self
    self.recordAudioService.delegate = self
  }
}

// MARK: - SleepTimerServiceProtocol
extension SleepTimerService: SleepTimerServiceProtocol {
  
  func startPlayingAudio() {
    self.playAudioService.play()
    self.state = .playing
    self.startTimer()
  }
  
  func pausePlayingAudio() {
    self.playAudioService.pause()
    self.state = .pausedPlaying
    self.stopTimer()
  }
  
  func stopPlayingAudio() {
    self.playAudioService.stop()
    self.state = .stopped
    self.stopTimer()
  }
  
  func startRecordingAudio() {
    self.recordAudioService.record()
    if self.canRecording {
      self.state = .recording
    }
  }
  
  func pauseRecordingAudio() {
    self.recordAudioService.pause()
    self.state = .pausedRecording
  }
  
  func stopRecordingAudio() {
    self.recordAudioService.stop()
    self.state = .stopped
  }
}

// MARK: - PlayAudioServiceDelegate
extension SleepTimerService: PlayAudioServiceDelegate {
  
  func playAudioServiceBeginInterruption(_ service: PlayAudioServiceProtocol) {
    self.pausePlayingAudio()
  }
  
  func playAudioServiceEndInterruption(_ service: PlayAudioServiceProtocol) {
    self.startPlayingAudio()
  }
}

// MARK: - RecordAudioServiceDelegate
extension SleepTimerService: RecordAudioServiceDelegate {
  
  func recordAudioServiceDidAuthorize(succeeded: Bool) {
    if !succeeded {
      self.delegate?.sleepTimerServiceRecordAudioDidFailAuthorization()
    }
  }
  
  func recordAudioServiceBeginInterruption(_ service: RecordAudioServiceProtocol) {
    self.pauseRecordingAudio()
  }
  
  func recordAudioServiceEndInterruption(_ service: RecordAudioServiceProtocol) {
    self.startRecordingAudio()
  }
}

// MARK: - Private methods
private extension SleepTimerService {
  
  func startTimer() {
    guard self.timer == nil else { return }
    self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (_) in
      self?.updateCounter()
    }
    self.timer?.tolerance = 0.1
  }
  
  func stopTimer() {
    self.timer?.invalidate()
    self.timer = nil
  }
  
  func updateCounter() {
    self.counter += 1
    
    guard let duration = self.duration else { return }
    if self.counter > duration {
      self.action = .off
      self.stopPlayingAudio()
      self.startRecordingAudio()
    }
  }
}
