//
//  AlarmService.swift
//  Booster
//
//  Created by Serhii Petrishenko on 18.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import Foundation

protocol AlarmServiceProtocol {
  var delegate: AlarmServiceDelegate? { get set }
  var time: Date { get set }
  var state: AlarmState { get }
  
  func startRunning()
  func stopRunning()
  func startPlayingAudio()
  func pausePlayingAudio()
}

protocol AlarmServiceDelegate: class {
  func alarmService(didChangeTime time: Date)
  func alarmService(didChangeState state: AlarmState)
}

class AlarmService {
  
  weak var delegate: AlarmServiceDelegate?
  
  var time: Date {
    didSet {
      self.delegate?.alarmService(didChangeTime: self.time)
    }
  }
  
  private(set) var state: AlarmState {
    didSet {
      self.delegate?.alarmService(didChangeState: self.state)
    }
  }
  
  private var playAudioService: PlayAudioServiceProtocol
  private var timer: Timer?
  
  private var duration: Int {
    return Int(self.time.timeIntervalSince(Date()))
  }
  
  init(soundUrl: URL) {
    self.time = Date(timeInterval: 120, since: Date().getDateWithZeroSeconds())
    self.state = .stopped
    self.playAudioService = PlayAudioService(fileUrl: soundUrl)
    self.playAudioService.delegate = self
  }
}

// MARK: - AlarmServiceProtocol
extension AlarmService: AlarmServiceProtocol {
  
  func startRunning() {
    self.startTimer()
    self.state = .running
  }
  
  func stopRunning() {
    self.playAudioService.stop()
    self.state = .stopped
  }
  
  func startPlayingAudio() {
    self.playAudioService.play()
  }
  
  func pausePlayingAudio() {
    self.playAudioService.pause()
  }
}

// MARK: - PlayAudioServiceDelegate
extension AlarmService: PlayAudioServiceDelegate {
  
  func playAudioServiceBeginInterruption(_ service: PlayAudioServiceProtocol) {
    self.pausePlayingAudio()
  }
  
  func playAudioServiceEndInterruption(_ service: PlayAudioServiceProtocol) {
    self.startPlayingAudio()
  }
}

// MARK: - Private methods
private extension AlarmService {
  
  func startTimer() {
    guard self.timer == nil else { return }
    let timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] (_) in
      self?.updateTimer()
    }
    timer.tolerance = 0.1
    RunLoop.current.add(timer, forMode: .common)
    self.timer = timer
  }

  func stopTimer() {
    self.timer?.invalidate()
    self.timer = nil
  }

  func updateTimer() {
    if self.duration == 0 {
      self.stopTimer()
      self.state = .wentOff
    }
  }
}
