//
//  TimeFormatter.swift
//  Booster
//
//  Created by Serhii Petrishenko on 18.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import Foundation

extension Date {
  func formattedString() -> String {
    let formatter = DateFormatter()
    formatter.dateStyle = .none
    formatter.timeStyle = .short
    return formatter.string(from: self)
  }
  
  func getDateWithZeroSeconds() -> Date {
    let timeInterval = floor(self.timeIntervalSinceReferenceDate / 60.0) * 60.0
    return Date(timeIntervalSinceReferenceDate: timeInterval)
  }
}
