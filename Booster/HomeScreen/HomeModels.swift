//
//  HomeModels.swift
//  Booster
//
//  Created by Serhii Petrishenko on 18.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import Foundation

enum HomeModel {
  
  struct TitleViewModel {
    let title: String
  }
  
  struct SleepTimerViewModel {
    let title: String
  }
  
  struct AlarmViewModel {
    let title: String
  }
  
  struct ActionViewModel {
    let title: String
  }
}
