//
//  HomeInteractor.swift
//  Booster
//
//  Created by Serhii Petrishenko on 18.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import Foundation

protocol HomeBusinessLogic {
  func loadDefaults()
  func playOrPause()
}

protocol HomeDataStore {
  var alertFactory: AlertFactoryProtocol { get }
  var applicationService: ApplicationServiceProtocol { get }
}

class HomeInteractor {
  
  var presenter: HomePresentationLogic?
  private(set) var alertFactory: AlertFactoryProtocol
  private(set) var applicationService: ApplicationServiceProtocol
  
  init() {
    self.alertFactory = AlertFactory()
    self.applicationService = ApplicationService()
    self.applicationService.delegate = self
  }
}

// MARK: - HomeBusinessLogic
extension HomeInteractor: HomeBusinessLogic {
  
  func loadDefaults() {
    self.presenter?.presentTitle(for: self.applicationService.getApplicationState())
    self.presenter?.presentSleepTimer(for: self.applicationService.getSleepTimerAction())
    self.presenter?.presentAlarm(for: self.applicationService.getAlarmTime())
    self.presenter?.presentAction(for: self.applicationService.getSleepTimerState())
  }
  
  func playOrPause() {
    self.applicationService.playOrPauseSleepTimer()
  }
}

// MARK: - ApplicationServiceDelegate
extension HomeInteractor: ApplicationServiceDelegate {
  
  func applicationService(didChangeApplicationState state: ApplicationState) {
    self.presenter?.presentTitle(for: state)
  }
  
  func applicationService(didChangeSleepTimerAction action: SleepTimerAction) {
    self.presenter?.presentSleepTimer(for: action)
  }
  
  func applicationService(didChangeSleepTimerState state: SleepTimerState) {
    self.presenter?.presentAction(for: state)
  }
  
  func applicationService(didChangeAlarmTime time: Date) {
    self.presenter?.presentAlarm(for: time)
  }
  
  func applicationServiceDidFailAuthorization() {
    self.presenter?.presentPermissionAlert()
  }
  
  func applicationServiceDidGoAlarmOff() {
    self.presenter?.presentStopAlarmAlert()
  }
}

// MARK: - HomeDataStore
extension HomeInteractor: HomeDataStore { }
