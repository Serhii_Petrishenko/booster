//
//  ViewController.swift
//  Booster
//
//  Created by Serhii Petrishenko on 17.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import UIKit

protocol HomeDisplayLogic: class {
  func displayTitle(viewModel: HomeModel.TitleViewModel)
  func displaySleepTimer(viewModel: HomeModel.SleepTimerViewModel)
  func displayAlarm(viewModel: HomeModel.AlarmViewModel)
  func displayAction(viewModel: HomeModel.ActionViewModel)
  func displayPermissionAlert()
  func displayStopAlarmAlert()
}

class HomeViewController: UIViewController {
  
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var sleepTimeButton: UIButton!
  @IBOutlet private weak var alarmButton: UIButton!
  @IBOutlet private weak var actionButton: UIButton!
  
  var interactor: HomeBusinessLogic?
  var router: (HomeRoutingLogic & HomeDataPassing)?
  
  private let alertFactory: AlertFactoryProtocol = AlertFactory()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.setup()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.configure()
    self.interactor?.loadDefaults()
  }
  
  @IBAction private func sleepTimeButtonDidPress(_ sender: Any) {
    self.router?.showSleepTimerController()
  }
  
  @IBAction private func alarmButtonDidPress(_ sender: Any) {
    self.router?.showAlarmController()
  }
  
  @IBAction private func actionButtonDidPress(_ sender: Any) {
    self.interactor?.playOrPause()
  }
  
}

// MARK: - HomeDisplayLogic
extension HomeViewController: HomeDisplayLogic {
  
  func displayTitle(viewModel: HomeModel.TitleViewModel) {
    self.titleLabel.text = viewModel.title
  }
  
  func displaySleepTimer(viewModel: HomeModel.SleepTimerViewModel) {
    self.sleepTimeButton.setTitle(viewModel.title, for: .normal)
  }
  
  func displayAlarm(viewModel: HomeModel.AlarmViewModel) {
    self.alarmButton.setTitle(viewModel.title, for: .normal)
  }
  
  func displayAction(viewModel: HomeModel.ActionViewModel) {
    self.actionButton.setTitle(viewModel.title, for: .normal)
  }
  
  func displayPermissionAlert() {
    self.router?.showPermissionController()
  }
  
  func displayStopAlarmAlert() {
    self.router?.showStopAlarmController()
  }
}

// MARK: - Private methods
private extension HomeViewController {
  
  func setup() {
    let viewController = self
    let interactor = HomeInteractor()
    let presenter = HomePresenter()
    let router = HomeRouter()
    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }
  
  func configure() {
    self.actionButton.layer.cornerRadius = 5
  }
}

