//
//  HomePresenter.swift
//  Booster
//
//  Created by Serhii Petrishenko on 18.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import Foundation

protocol HomePresentationLogic {
  func presentTitle(for state: ApplicationState)
  func presentSleepTimer(for action: SleepTimerAction)
  func presentAlarm(for date: Date)
  func presentAction(for state: SleepTimerState)
  func presentPermissionAlert()
  func presentStopAlarmAlert()
}

class HomePresenter {
  
  weak var viewController: HomeDisplayLogic?
  
}

// MARK: - HomePresentationLogic
extension HomePresenter: HomePresentationLogic {
  
  func presentTitle(for state: ApplicationState) {
    let viewModel = HomeModel.TitleViewModel(title: state.description)
    self.viewController?.displayTitle(viewModel: viewModel)
  }
  
  func presentSleepTimer(for action: SleepTimerAction) {
    let viewModel = HomeModel.SleepTimerViewModel(title: action.description)
    self.viewController?.displaySleepTimer(viewModel: viewModel)
  }
  
  func presentAlarm(for date: Date) {
    let viewModel = HomeModel.AlarmViewModel(title: date.formattedString())
    self.viewController?.displayAlarm(viewModel: viewModel)
  }
  
  func presentAction(for state: SleepTimerState) {
    let viewModel = HomeModel.ActionViewModel(title: state.description)
    self.viewController?.displayAction(viewModel: viewModel)
  }
  
  func presentPermissionAlert() {
    self.viewController?.displayPermissionAlert()
  }
  
  func presentStopAlarmAlert() {
    self.viewController?.displayStopAlarmAlert()
  }
}
