//
//  Router.swift
//  Booster
//
//  Created by Serhii Petrishenko on 18.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import UIKit

protocol HomeRoutingLogic {
  func showSleepTimerController()
  func showAlarmController()
  func showPermissionController()
  func showStopAlarmController()
}

protocol HomeDataPassing {
  var dataStore: HomeDataStore? { get set }
}

class HomeRouter {
  weak var viewController: HomeViewController?
  var dataStore: HomeDataStore?
}

// MARK: - HomeRoutingLogic
extension HomeRouter: HomeRoutingLogic {
  
  func showSleepTimerController() {
    guard let dataStore = self.dataStore else { return }
    
    let sleepTimeController = dataStore.alertFactory.sleepTimerController { (action) in
      dataStore.applicationService.setSleepTimerAction(action)
    }
    self.present(sleepTimeController)
  }
  
  func showAlarmController() {
    guard let dataStore = self.dataStore else { return }
    
    let alarmController = dataStore.alertFactory.alarmController(date: dataStore.applicationService.getAlarmTime()) { (date) in
      dataStore.applicationService.setAlarmTime(date)
    }
    self.present(alarmController)
  }
  
  func showPermissionController() {
    guard let dataStore = self.dataStore else { return }
    
    let permissionController = dataStore.alertFactory.permissionController {
      let url = URL(string: UIApplication.openSettingsURLString)!
      UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    self.present(permissionController)
  }
  
  func showStopAlarmController() {
    guard let dataStore = self.dataStore else { return }
    
    let stopAlarmController = dataStore.alertFactory.stopAlarmController {
      dataStore.applicationService.stopAlarm()
    }
    self.present(stopAlarmController)
  }
}

// MARK: - HomeDataPassing
extension HomeRouter: HomeDataPassing { }

// MARK: - Private methods
private extension HomeRouter {
  
  func present(_ controller: UIViewController) {
    self.viewController?.present(controller, animated: true)
  }
}
