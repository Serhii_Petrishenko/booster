//
//  AlarmAlertController.swift
//  Booster
//
//  Created by Serhii Petrishenko on 17.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import UIKit

class AlarmAlertController: UIAlertController {
  
  private lazy var timePicker: UIDatePicker = {
    let picker = UIDatePicker()
    picker.datePickerMode = .time
    return picker
  }()
  
  convenience init(date: Date, title: String, action: ((Date) -> ())?) {
    self.init(title: title, message: nil, preferredStyle: .actionSheet)
    self.view.addSubview(self.timePicker)
    self.timePicker.setDate(date, animated: false)
    self.timePicker.translatesAutoresizingMaskIntoConstraints = false
    self.timePicker.heightAnchor.constraint(equalToConstant: 200).isActive = true
    self.timePicker.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 50).isActive = true
    self.timePicker.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -120).isActive = true
    self.timePicker.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
    self.timePicker.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
    
    let doneActrion = UIAlertAction(title: "Done", style: .default) { (_) in
      action?(self.timePicker.date)
    }
    self.addAction(doneActrion)
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    self.addAction(cancelAction)
  }
}
