//
//  SleepTimerAlertController.swift
//  Booster
//
//  Created by Serhii Petrishenko on 17.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import UIKit

class SleepTimerAlertAction: UIAlertAction {
  convenience init(type: SleepTimerAction, handler: ((SleepTimerAction) -> ())?) {
    self.init(title: type.description, style: .default) { (_) in
      handler?(type)
    }
  }
}

class SleepTimerAlertController: UIAlertController {
  convenience init(title: String, items: [SleepTimerAction], handler: ((SleepTimerAction) -> ())?) {
    self.init(title: title, message: nil, preferredStyle: .actionSheet)
    items.forEach {
      let action = SleepTimerAlertAction(type: $0, handler: handler)
      self.addAction(action)
    }
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    self.addAction(cancelAction)
  }
}
