//
//  AlertAction.swift
//  Booster
//
//  Created by Serhii Petrishenko on 17.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

enum SleepTimerAction {
  case on(Int)
  case off
  
  var canPlaySound: Bool {
    switch self.self {
    case .off: return false
    case .on: return true
    }
  }
}

extension SleepTimerAction: Equatable {
  static func ==(lhs: SleepTimerAction, rhs: SleepTimerAction) -> Bool {
    switch (lhs, rhs) {
    case (let .on(lhsValue), let .on(rhsValue)):
      return lhsValue == rhsValue
    case (.off, off):
      return true
    default:
      return false
    }
  }
}

extension SleepTimerAction: CustomStringConvertible {
  var description: String {
    switch self {
    case .off: return "Off"
    case .on(let value): return String(value) + " min"
    }
  }
}
