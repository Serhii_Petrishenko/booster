//
//  SleepTimerState.swift
//  Booster
//
//  Created by Serhii Petrishenko on 18.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import Foundation

enum SleepTimerState {
  case playing
  case recording
  case pausedPlaying
  case pausedRecording
  case stopped
}

extension SleepTimerState: CustomStringConvertible {
  var description: String {
    switch self {
    case .playing, .recording: return "Pause"
    case .pausedPlaying, .pausedRecording, .stopped: return "Play"
    }
  }
}
