//
//  AlarmState.swift
//  Booster
//
//  Created by Serhii Petrishenko on 19.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import Foundation

enum AlarmState {
  case running
  case wentOff
  case stopped
}
