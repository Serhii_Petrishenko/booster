//
//  ApplicationState.swift
//  Booster
//
//  Created by Serhii Petrishenko on 17.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

enum ApplicationState {
  case iddle
  case playing
  case recording
  case paused
  case alarm
  
  mutating func update(with state: SleepTimerState) {
    switch state {
    case .pausedPlaying, .pausedRecording:
      self = .paused
    case .playing:
      self = .playing
    case .recording:
      self = .recording
    case .stopped:
      self = .iddle
    }
  }
  
  mutating func update(with state: AlarmState) {
    switch state {
    case .stopped:
      self = .iddle
    case .wentOff:
      self = .alarm
    default:
      break
    }
  }
}

extension ApplicationState: CustomStringConvertible {
  var description: String {
    switch self {
    case .iddle: return "Iddle"
    case .playing: return "Playing"
    case .recording: return "Recording"
    case .paused: return "Paused"
    case .alarm: return "Alarm"
    }
  }
}
