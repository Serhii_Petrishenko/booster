//
//  AlertFactory.swift
//  Booster
//
//  Created by Serhii Petrishenko on 17.01.2020.
//  Copyright © 2020 Serhii Petrishenko. All rights reserved.
//

import UIKit

protocol AlertFactoryProtocol {
  func sleepTimerController(action: ((SleepTimerAction) -> ())?) -> UIAlertController
  func stopAlarmController(action: (() -> ())?) -> UIAlertController
  func alarmController(date: Date, action: ((Date) -> ())?) -> UIAlertController
  func permissionController(action: (() -> ())?) -> UIAlertController
}

class AlertFactory { }

// MARK: - AlertFactoryProtocol
extension AlertFactory: AlertFactoryProtocol {
  
  func sleepTimerController(action: ((SleepTimerAction) -> ())?) -> UIAlertController {
    let title = "Sleep Timer"
    let items: [SleepTimerAction] = [.off, .on(1), .on(5), .on(10), .on(15), .on(20)]
    let alertController = SleepTimerAlertController(title: title, items: items, handler: action)
    return alertController
  }
  
  func stopAlarmController(action: (() -> ())?) -> UIAlertController {
    let title = "Alarm went off"
    let cancelAction = UIAlertAction(title: "Stop", style: .cancel) { (_) in
      action?()
    }
    let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
    alertController.addAction(cancelAction)
    return alertController
  }
  
  func alarmController(date: Date, action: ((Date) -> ())?) -> UIAlertController {
    let title = "Alarm"
    let alertController = AlarmAlertController(date: date, title: title, action: action)
    return alertController
  }
  
  func permissionController(action: (() -> ())?) -> UIAlertController {
    let title = "Need permission"
    let message = "App needs permission for microphone"
    let cancelAction = UIAlertAction(title: "OK", style: .cancel) { (_) in
      action?()
    }
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alertController.addAction(cancelAction)
    return alertController
  }
}
